%%%%%%%%% Load Test Load %%%%%%%%
[x,fs]=audioread('Gustavo_Cerati.wav');
Fs=40000;
fp= 3000;
fr= 2000;
Rp= 3;
Rr=10;
Ts=1/Fs;
wp= 2*pi*(fp/Fs);
wr=2*pi*(fr/Fs);
%%%%%%%%%%%%%%Dise�o del filtro %%%%%%%%%%%%%%%%
wpanalog= (2/Ts)*tan(wp/2);
wranalog= (2/Ts)*tan(wr/2);
[N,WN] = cheb2ord(wpanalog,wranalog,Rp,Rr,'s');
%[N,WN]= buttord(wpanalog,wranalog,Rp,Rr,'s');

[BS,AS]=cheby2(N,Rr,WN,'high','s');
w=logspace(2,7,500);
figure;freqs(BS,AS,w);
[BZ,AZ]=bilinear(BS,AS,Fs);%%%%%%%%%%% Transfer function %%%
figure;freqz(BZ,AZ);
sys=tf(BZ,AZ,Ts);
y3=filter(BZ,AZ,x);
y_filtro3=audioplayer(y3,Fs);


