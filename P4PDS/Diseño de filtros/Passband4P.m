%%%%%%%%%%% Passband conditions %%%%%%
[x,Fs]=audioread('Gustavo_Cerati.wav');
fs=40e3;
fp1=800;
fp2= 1200;
fr1= 500;
fr2= 1500;
Rp=1;
Rr=3;
Ts=1/Fs;
wp1=2*pi*(fp1/Fs);
wp2=2*pi*(fp2/Fs);
wr1=2*pi*(fr1/Fs);
wr2= 2*pi*(fr2/Fs);
%%%%%%%%%%%%%%Dise�o del filtro %%%%%%%%%%%%%%%%
wpanalog1= (2/Ts)*tan(wp1/2);
wpanalog2= (2/Ts)*tan(wp2/2);

wranalog1= (2/Ts)*tan(wr1/2);
wranalog2= (2/Ts)*tan(wr2/2);

[N,WN]= cheb2ord([wpanalog1 wpanalog2] ,[wranalog1 wranalog2],Rp,Rr,'s');
[BS,AS]=cheby2(N,Rr,WN,'s');
w=logspace(2,7,500);
figure;freqs(BS,AS,w);
[BZ,AZ]=bilinear(BS,AS,Fs);
figure;freqz(BZ,AZ);
sys=tf(BZ,AZ,Ts);
y2=filter(BZ,AZ,x);
y_filtro2=audioplayer(y2,Fs);
%play(y_filtro1);




