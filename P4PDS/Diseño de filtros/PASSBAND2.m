[x,Fs]=audioread('Gustavo_Cerati.wav');
fs=40e3;
fp1=900;
fp2= 1400;
fr1= 600;
fr2= 1800;
Rp=3;
Rr=39;
Ts=1/Fs;
wp1=2*pi*(fp1/Fs);
wp2=2*pi*(fp2/Fs);
wr1=2*pi*(fr1/Fs);
wr2= 2*pi*(fr2/Fs);

wpanalog1= (2/Ts)*tan(wp1/2);
wpanalog2= (2/Ts)*tan(wp2/2);

wranalog1= (2/Ts)*tan(wr1/2);
wranalog2= (2/Ts)*tan(wr2/2);
[N,WN]= ellipord([wpanalog1 wpanalog2] ,[wranalog1 wranalog2],Rp,Rr,'s');
[BS,AS]=ellip(N,Rp,Rr,WN,'s');
w=logspace(2,7,500);
figure;freqs(BS,AS,w);
[BZ,AZ]=bilinear(BS,AS,Fs);
figure;freqz(BZ,AZ);
sys=tf(BZ,AZ,Ts);
y2=filter(BZ,AZ,x);
y_filtro2=audioplayer(y2,Fs);

