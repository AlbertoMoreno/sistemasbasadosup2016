[x,Fs]=audioread('Gustavo_Cerati.wav');
fs=40e3;
fp=800;%1200
fr=500;%400%1700
Rp=3;
Rr=10;%20
Ts=1/fs;
wp= 2*pi*(fp/fs);
wr=2*pi*(fr/fs);
%%%%%%%%%%%%%%Dise�o del filtro %%%%%%%%%%%%%%%%
wpanalog= (2/Ts)*tan(wp/2);
wranalog= (2/Ts)*tan(wr/2);
% [N,WN]= buttord(wpanalog,wranalog,Rp,Rr,'s');
% [BS,AS]=butter(N,WN,'s');
% w=logspace(2,7,500);
% figure;freqs(BS,AS,w);
% [BZ,AZ]=bilinear(BS,AS,fs);%%%%%%%%%%% Transfer function %%%
% figure;freqz(BZ,AZ);
% sys=tf(BZ,AZ,Ts);
% y1=filter(BZ,AZ,x);
% y_filtro1=audioplayer(y1,fs);
[N,WN] = cheb2ord(wpanalog,wranalog,Rp,Rr,'s');
%[N,WN]= buttord(wpanalog,wranalog,Rp,Rr,'s');

[BS,AS]=cheby2(N,Rr,WN,'high','s');
w=logspace(2,7,500);
figure;freqs(BS,AS,w);
[BZ,AZ]=bilinear(BS,AS,Fs);%%%%%%%%%%% Transfer function %%%
figure;freqz(BZ,AZ);
sys=tf(BZ,AZ,Ts);
y3=filter(BZ,AZ,x);
y_filtro3=audioplayer(y3,Fs);
